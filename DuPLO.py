# -*- coding: utf-8 -*-
import sys
import time
import os
import tensorflow as tf
import numpy as np
from sklearn.utils import shuffle
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import accuracy_score,f1_score,cohen_kappa_score,log_loss

def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return batch

def format_X (X,n_timestamps):
    X = X.reshape(X.shape[0],5,5,-1,n_timestamps)
    print (X.shape)
    return X

def format_y (y,n_classes,onehot=True,target=-1):
    encoder = LabelEncoder()
    y_tr = encoder.fit_transform(y[:,target])
    if onehot :
        y_tr = tf.keras.utils.to_categorical(y_tr, n_classes)
    print (y_tr.shape)
    return y_tr

def transform_label(test_label,test_prediction):
    encoder = LabelEncoder()
    encoder.fit(test_label)
    prediction = encoder.inverse_transform(test_prediction)
    print (prediction.shape)
    return prediction

def cnn (X, n_filters, drop, is_training):
    conv1 = tf.keras.layers.Conv2D(filters=n_filters,kernel_size=[3,3],padding="valid",activation=tf.nn.relu)(X)
    conv1 = tf.keras.layers.BatchNormalization()(conv1)
    conv1 = tf.keras.layers.Dropout(rate = drop)(conv1,training=is_training)

    conv2 = tf.keras.layers.Conv2D(filters=n_filters*2,kernel_size=[3,3],padding="valid",activation=tf.nn.relu)(conv1)
    conv2 = tf.keras.layers.BatchNormalization()(conv2)
    conv2 = tf.keras.layers.Dropout(rate = drop)(conv2,training=is_training)

    conv3 = tf.keras.layers.Conv2D(filters=n_filters*4,kernel_size=[1,1],padding="valid",activation=tf.nn.relu)(conv2)
    conv3 = tf.keras.layers.BatchNormalization()(conv3)
    conv3 = tf.keras.layers.Dropout(rate = drop)(conv3,training=is_training)

    flatten = tf.keras.layers.Flatten()(conv3)
    return flatten

def scnn (X, n_filters):
    conv1 = tf.keras.layers.Conv2D(filters=n_filters,kernel_size=[3,3],padding="valid",activation=tf.nn.relu)(X)
    conv1 = tf.keras.layers.BatchNormalization()(conv1)

    conv2 = tf.keras.layers.Conv2D(filters=n_filters*2,kernel_size=[3,3],padding="valid",activation=tf.nn.relu)(conv1)
    conv2 = tf.keras.layers.BatchNormalization()(conv2)

    flatten = tf.keras.layers.Flatten()(conv2)
    return flatten

def attention_mechanism(H,att_units,cell_units):
    W = tf.Variable(tf.random.normal([cell_units, att_units], stddev=0.1))
    b = tf.Variable(tf.random.normal([att_units], stddev=0.1))
    u = tf.Variable(tf.random.normal([att_units], stddev=0.1))

    v = tf.tanh(tf.tensordot(H, W, axes=1) + b)
    linear_lambdas = tf.tensordot(v, u, axes=1)
    lambdas = tf.nn.softmax(linear_lambdas,name="lambdas")

    output = tf.reduce_sum(H * tf.expand_dims(lambdas, -1), 1)
    output = tf.reshape(output,[-1,cell_units])
    return output

def rnn (X,units,drop,is_training) :
    cell = tf.contrib.rnn.GRUCell(units)
    cell = tf.contrib.rnn.DropoutWrapper(cell, output_keep_prob=1-drop, state_keep_prob=1-drop)

    outputs, _ = tf.contrib.rnn.static_rnn(cell, X, dtype="float32")
    outputs = tf.stack(outputs, axis=1)
    output = attention_mechanism(outputs,units,units)
    return output

def run (train_X,train_y,valid_X,valid_y,model_directory,n_units,fc_units,cnn_filters,scnn_filters,batch_size,n_epochs,learning_rate,drop,n_timestamps,n_classes):

    n_bands = train_X.shape[3]

    X = tf.compat.v1.placeholder(tf.float32,[None,5,5,n_bands,n_timestamps],name="X")
    y = tf.compat.v1.placeholder(tf.float32,[None,n_classes], name="y")
    dropOut = tf.compat.v1.placeholder(tf.float32, shape=(), name="dropout")
    is_training = tf.compat.v1.placeholder(tf.bool, shape=(), name="is_training")

    X_list = tf.unstack(X,axis=-1)

    # CNN
    X_cnn = tf.concat(X_list,axis=3)
    cnn_feat = cnn (X_cnn,cnn_filters,drop,is_training=is_training)
    cnn_pred = tf.keras.layers.Dense(n_classes, activation=None)(cnn_feat)

    # RNN
    rnn_list = []
    for i in range (len(X_list)) :
        scnn_feat = scnn(X_list[i], scnn_filters)
        rnn_list.append(scnn_feat)

    # X_rnn = tf.stack(rnn_list,axis=1)
    rnn_feat = rnn(rnn_list,n_units,drop,is_training)
    rnn_pred = tf.keras.layers.Dense(n_classes, activation=None)(rnn_feat)

    # Features Concatenation
    concat_feat = tf.concat([cnn_feat,rnn_feat],axis=1,name="concat_features")
    concat_pred = tf.keras.layers.Dense(fc_units, activation=tf.nn.relu)(concat_feat)
    concat_pred = tf.keras.layers.Dense(fc_units, activation=tf.nn.relu)(concat_pred)
    concat_pred = tf.keras.layers.Dense(n_classes, activation=None)(concat_pred)

    weight = .3

    # Prediction
    with tf.compat.v1.variable_scope("pred"):
        score_tot = tf.nn.softmax(concat_pred)
        score_tot += weight*tf.nn.softmax(cnn_pred)
        score_tot += weight*tf.nn.softmax(rnn_pred)
        prediction = tf.math.argmax(score_tot,1, name="prediction")
        correct = tf.math.equal(tf.math.argmax(score_tot,1),tf.math.argmax(y,1))
        accuracy = tf.reduce_mean(tf.dtypes.cast(correct,tf.float64))

    # Cost and Auxiliary classifiers
    with tf.compat.v1.variable_scope("cost"):
        cost = tf.reduce_mean(tf.compat.v1.nn.softmax_cross_entropy_with_logits_v2(labels=y,logits=concat_pred))
        cost += weight*tf.reduce_mean(tf.compat.v1.nn.softmax_cross_entropy_with_logits_v2(labels=y,logits=cnn_pred))
        cost += weight*tf.reduce_mean(tf.compat.v1.nn.softmax_cross_entropy_with_logits_v2(labels=y,logits=rnn_pred))

    # Optimizer
    with tf.compat.v1.variable_scope("optimizer"):
        optimizer = tf.compat.v1.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    # Create a session and run the graph on training data
    n_batch = int(train_X.shape[0]/batch_size)
    if train_X.shape[0] % batch_size != 0:
        n_batch+=1
    print ("n_batch: %d" %n_batch)

    saver = tf.compat.v1.train.Saver()
    best_score = sys.float_info.min

    init = tf.compat.v1.global_variables_initializer()
    with tf.compat.v1.Session() as session:
        session.run(init)

        for epoch in range(1,n_epochs+1):
            start = time.time()
            epoch_loss = 0
            epoch_acc = 0

            train_X, train_y = shuffle(train_X, train_y, random_state=0)

            for batch in range(n_batch):
                batch_X = get_batch(train_X,batch,batch_size)
                batch_y = get_batch(train_y,batch,batch_size)

                acc, loss, _ = session.run([accuracy, cost, optimizer], feed_dict={X:batch_X,
                                                                                   y:batch_y,
                                                                                   dropOut:drop,
                                                                                   is_training:True})
                del batch_X, batch_y

                epoch_loss += loss
                epoch_acc += acc

            stop = time.time()
            elapsed = stop - start
            print ("Epoch ",epoch, " Train loss:",epoch_loss/n_batch,"| Accuracy:",epoch_acc/n_batch, "| Time: ",elapsed)

            # At each epoch run the model on validation set and save it if F1 score is better
            valid_batch = int(valid_X.shape[0] / (4*batch_size))
            if valid_X.shape[0] % (4*batch_size) != 0:
                valid_batch+=1

            total_pred = None
            for ibatch in range(valid_batch):
                valid_batch_X = get_batch(valid_X,ibatch,4*batch_size)

                batch_pred = session.run(prediction,feed_dict={X:valid_batch_X,
                                                               dropOut:0.,
                                                               is_training:False})
                del valid_batch_X

                if total_pred is None :
                    total_pred = batch_pred
                else :
                    total_pred = np.hstack((total_pred,batch_pred))

            val_score = accuracy_score(valid_y, total_pred)
            if val_score > best_score :
                print (np.bincount(np.array(total_pred)))
                print (np.bincount(np.array(valid_y)))
                print ("VALIDATION F1 Score: %f" % f1_score(valid_y, total_pred, average='weighted'))
                print (f1_score(valid_y, total_pred, average=None))

                save_path = saver.save(session, model_directory +"/model")
                print ("Model saved in path: %s" % save_path)
                best_score = val_score

def restore_prediction (test_X, test_y, model_directory, batch_size):
    ckpt_path = os.path.join(model_directory,"model")
    results_path = os.path.join(model_directory,"results")
    if not os.path.exists(results_path):
        os.makedirs(results_path)

    tf.compat.v1.reset_default_graph()
    with tf.compat.v1.Session() as session :
        model_saver = tf.compat.v1.train.import_meta_graph(ckpt_path+".meta")
        model_saver.restore(session, ckpt_path)

        graph = tf.compat.v1.get_default_graph()
        X = graph.get_tensor_by_name("X:0")
        dropOut = graph.get_tensor_by_name("dropout:0")
        is_training = graph.get_tensor_by_name("is_training:0")
        prediction = graph.get_tensor_by_name("pred/prediction:0")
        print ("Model restored")

        n_batch = int(test_X.shape[0] / (4*batch_size))
        if test_X.shape[0] % (4*batch_size) != 0:
            n_batch+=1
        print ("n_batch: %d" %n_batch)

        total_pred = None

        for batch in range(n_batch):
            batch_X = get_batch(test_X,batch,(4*batch_size))

            batch_pred = session.run(prediction,feed_dict={X:batch_X, dropOut:0.,is_training:False})
            del batch_X

            if total_pred is None :
                total_pred = batch_pred
            else :
                total_pred = np.hstack((total_pred,batch_pred))

        total_pred = transform_label(test_y,total_pred)
        np.save(os.path.join(results_path,"predictions.npy"),total_pred)

def restore_features (train_X, valid_X, test_X, model_directory, batch_size):
    ckpt_path = os.path.join(model_directory,"model")
    results_path = os.path.join(model_directory,"results")
    if not os.path.exists(results_path):
        os.makedirs(results_path)

    tf.compat.v1.reset_default_graph()
    with tf.compat.v1.Session() as session :
        model_saver = tf.compat.v1.train.import_meta_graph(ckpt_path+".meta")
        model_saver.restore(session, ckpt_path)

        graph = tf.compat.v1.get_default_graph()
        X = graph.get_tensor_by_name("X:0")
        dropOut = graph.get_tensor_by_name("dropout:0")
        is_training = graph.get_tensor_by_name("is_training:0")
        learnt_features = graph.get_tensor_by_name("concat_features:0")
        print ("Model restored")

        # train
        n_batch = int(train_X.shape[0] / (4*batch_size))
        if train_X.shape[0] % (4*batch_size) != 0:
            n_batch+=1
        print ("n_batch: %d" %n_batch)
        train_learnt_feat = None
        for batch in range(n_batch):
            batch_X = get_batch(train_X,batch,(4*batch_size))

            batch_learnt_feat = session.run(learnt_features,feed_dict={X:batch_X, dropOut:0.,is_training:False})
            del batch_X

            if train_learnt_feat is None :
                train_learnt_feat = batch_learnt_feat
            else :
                train_learnt_feat = np.vstack((train_learnt_feat,batch_learnt_feat))
        np.save(os.path.join(results_path,"train_learnt_features.npy"),train_learnt_feat)

        # validation
        n_batch = int(valid_X.shape[0] / (4*batch_size))
        if valid_X.shape[0] % (4*batch_size) != 0:
            n_batch+=1
        print ("n_batch: %d" %n_batch)
        valid_learnt_feat = None
        for batch in range(n_batch):
            batch_X = get_batch(valid_X,batch,(4*batch_size))
            batch_learnt_feat = session.run(learnt_features,feed_dict={X:batch_X, dropOut:0.,is_training:False})
            del batch_X

            if valid_learnt_feat is None :
                valid_learnt_feat = batch_learnt_feat
            else :
                valid_learnt_feat = np.vstack((valid_learnt_feat,batch_learnt_feat))
        np.save(os.path.join(results_path,"valid_learnt_features.npy"),valid_learnt_feat)

        # test
        n_batch = int(test_X.shape[0] / (4*batch_size))
        if test_X.shape[0] % (4*batch_size) != 0:
            n_batch+=1
        print ("n_batch: %d" %n_batch)
        test_learnt_feat = None
        for batch in range(n_batch):
            batch_X = get_batch(test_X,batch,(4*batch_size))
            batch_learnt_feat = session.run(learnt_features,feed_dict={X:batch_X, dropOut:0.,is_training:False})
            del batch_X

            if test_learnt_feat is None :
                test_learnt_feat = batch_learnt_feat
            else :
                test_learnt_feat = np.vstack((test_learnt_feat,batch_learnt_feat))
        np.save(os.path.join(results_path,"test_learnt_features.npy"),test_learnt_feat)

if __name__ == "__main__":
    # print (tf.__version__)
    # Reading data
    train_X = np.load(sys.argv[1])
    valid_X = np.load(sys.argv[2])
    test_X = np.load(sys.argv[3])
    train_y = np.load(sys.argv[4])
    valid_y = np.load(sys.argv[5])
    test_y = np.load(sys.argv[6])

    model_directory = sys.argv[7]
    if not os.path.exists(model_directory):
        os.makedirs(model_directory)
    
    n_timestamps = int(sys.argv[8])
    n_classes = int(sys.argv[9])
    sys.stdout.flush

    # Formatting
    train_X = format_X (train_X,n_timestamps)
    valid_X = format_X (valid_X,n_timestamps)
    test_X = format_X (test_X,n_timestamps)

    train_y = format_y (train_y,n_classes)
    valid_y = format_y (valid_y,n_classes,onehot=False)

    # Model Parameters
    rnn_units = 128
    fc_units = 128
    cnn_filters = 32
    scnn_filters = 32
    batch_size = 32
    n_epochs = 3000
    learning_rate = 2E-4
    drop = 0.4

    # Run Model
    run (train_X,train_y,valid_X,valid_y,model_directory,rnn_units,fc_units,cnn_filters,scnn_filters,batch_size,n_epochs,learning_rate,drop,n_timestamps,n_classes)
    restore_prediction (test_X,test_y,model_directory,batch_size)