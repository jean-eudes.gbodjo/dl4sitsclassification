import sys
import os
import argparse
from pathlib import Path
import numpy as np
from utils import format_y
from train import run
from test import restore
from models.tempcnn import TempCNN_Model
from models.hob2srnn import HOb2sRNN_Model

def boolean_string(s):
    if s not in {'False', 'True'}:
        raise ValueError('Not a valid boolean string')
    return s == 'True'

if __name__ == '__main__':
    
    # Parsing arguments
    if len(sys.argv) == 1:
        print ('Usage: python '+sys.argv[0]+' train_X train_y valid_X valid_y test_X test_y [options]' )
        print ('Help: python '+sys.argv[0]+' -h/--help')
        sys.exit(1)

    parser = argparse.ArgumentParser()
    parser.add_argument('data_path',help='Path to data',type=str)
    parser.add_argument('gt_path',help='Path to label',type=str)
    parser.add_argument('num_split',help='Number of split to use',type=str)
    parser.add_argument('-m','--model',help='Which model to execute',choices=['tempcnn','hob2srnn'],default='tempcnn',type=str)
    parser.add_argument('-out','--out_path',help='Output path for model and results',type=str)
    parser.add_argument('-bs','--batch_size',dest='batch_size',help='Batch size',default=32,type=int)
    parser.add_argument('-ep','--num_epochs',dest='num_epochs',help='Number of training epochs',default=1000,type=int)
    parser.add_argument('-lr','--learning_rate',dest='learning_rate',help='Learning rate',default=1e-4,type=float)
    parser.add_argument('-tqdm',dest='tqdm',help='Display tqdm progress bar',default=False,type=boolean_string)
    args = parser.parse_args()

    # Get argument values
    data_path = args.data_path
    gt_path = args.gt_path
    n_split = args.num_split
    model_to_train = args.model

    if not args.out_path is None :
        out_path = args.out_path
    else:
        out_path = f'model_{model_to_train}'
    batch_size = args.batch_size
    n_epochs = args.num_epochs
    lr = args.learning_rate
    tqdm_display = args.tqdm
    
    # Create output path if does not exist
    Path(out_path).mkdir(parents=True, exist_ok=True)

    # Load Training and Validation set
    train_y = format_y(os.path.join(gt_path,f'train_gt_{n_split}.npy') )
    print ('Training GT:',train_y.shape)
    valid_y = format_y(os.path.join(gt_path,f'valid_gt_{n_split}.npy') )
    print ('Validation GT:', valid_y.shape)
    n_classes = len(np.unique(train_y))
    print ('Number of classes:',n_classes)

    train_X = np.load(os.path.join(data_path,f'train_S2_{n_split}.npy') )
    print ('Training X:',train_X.shape)
    valid_X = np.load(os.path.join(data_path,f'valid_S2_{n_split}.npy') )
    print ('Validation X:',valid_X.shape)

    # Create the object model
    if model_to_train == 'tempcnn':
        model = TempCNN_Model(n_classes)
    elif model_to_train == 'hob2srnn':
        model = HOb2sRNN_Model(n_classes)
    
    # Learning stage
    checkpoint_path = os.path.join(out_path,f'model_{n_split}')
    run (model,train_X,train_y,valid_X,valid_y,checkpoint_path,batch_size,
            lr,n_epochs,tqdm_display)

    # Load Test set 
    test_y = format_y(os.path.join(gt_path,f'test_gt_{n_split}.npy'), encode=False )
    print ('Test GT:',test_y.shape)

    test_X = np.load(os.path.join(data_path,f'test_S2_{n_split}.npy') )
    print ('Test X:',test_X.shape)

    # Inference stage
    result_path = os.path.join(out_path,f'pred_{n_split}.npy')
    restore (model,test_X,test_y,batch_size,checkpoint_path,result_path,tqdm_display)