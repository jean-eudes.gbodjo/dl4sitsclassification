#!/bin/bash

export CUDA_VISIBLE_DEVICES=3

gt_path='../Knowledge_Distillation/data_v2/gt'
s2_path='../Knowledge_Distillation/data_v2/S2'

python main.py $s2_path $gt_path 1 -tqdm True -ep 2 -bs 256 -m hob2srnn