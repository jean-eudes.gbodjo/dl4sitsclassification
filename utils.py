import numpy as np
from sklearn.preprocessing import LabelEncoder

def format_y (array_path,encode=True):
    '''
    Format ground truth data
    Encode label (second column) with values between 0 and n_classes-1.
    output shape: (number of samples,)
    '''
    array = np.load(array_path)[:,1]
    if encode :
        encoder = LabelEncoder()
        array = encoder.fit_transform( array )
    return array

def transform_y (y,prediction):
    '''
    Transform labels back to original encoding
    output shape: (number of samples,)
    '''
    encoder = LabelEncoder()
    encoder.fit(y)
    return encoder.inverse_transform(prediction)

def get_iteration (array, batch_size):
    '''
    Function to get the number of iterations over one epoch w.r.t batch size
    '''
    n_batch = int(array.shape[0]/batch_size)
    if array.shape[0] % batch_size != 0:
        n_batch+=1
    return n_batch

def get_batch (array, i, batch_size):
    '''
    Function to select batch of training/validation/test set
    '''
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return batch