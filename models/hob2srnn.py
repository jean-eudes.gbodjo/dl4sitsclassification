import tensorflow as tf
from tensorflow.keras.layers import Layer, Dense, Activation, Dropout, BatchNormalization, RNN
tf.keras.backend.set_floatx('float32')

class FCGRU (tf.keras.layers.SimpleRNNCell):
    '''
	Gated Recurrent Unit cell (http://arxiv.org/abs/1406.1078) 
    enriched with Fully Connected layers
	'''
    def __init__(self,units,fc_units,drop=0):
        super(FCGRU, self).__init__(units)

        self.fc_units = fc_units
        self.dense1 = Dense(fc_units,activation='tanh')
        self.drop1 = Dropout(rate=drop)
        self.dense2 = Dense(fc_units*2,activation='tanh')
        self.drop2 = Dropout(rate=drop)
        self.drop3 = Dropout(rate=drop)

    def build(self,input_shapes):
        self.b_g1 = self.add_weight(name='b_g1', shape=(self.units,))
        self.b_g2 = self.add_weight(name='b_g2', shape=(self.units,))
        self.b_g3 = self.add_weight(name='b_g3', shape=(self.units,))

        self.weights_g1 = self.add_weight(name='weights_g1', shape=(self.fc_units*2, self.units))
        self.weights_g2 = self.add_weight(name='weights_g2', shape=(self.fc_units*2, self.units))
        self.weights_g3 = self.add_weight(name='weights_g3', shape=(self.fc_units*2, self.units))

        self.weights_g1h = self.add_weight(name='weights_g1_h', shape=(self.units, self.units))
        self.weights_g2h = self.add_weight(name='weights_g2_h', shape=(self.units, self.units))
        self.weights_g3h = self.add_weight(name='weights_g3_h', shape=(self.units, self.units))

    def call(self, inputs, state, training):        
        # FC Layers
        fc1 = self.dense1(inputs)
        fc1 = self.drop1(fc1,training)
        fc2 = self.dense2(fc1)
        fc2 = self.drop2(fc2,training)

        # Update Gate
        zt = tf.math.sigmoid( tf.matmul(fc2, self.weights_g1) + tf.matmul(state[0], self.weights_g1h) + self.b_g1) 
        # Reset Gate
        rt = tf.math.sigmoid( tf.matmul(fc2, self.weights_g2) + tf.matmul(state[0], self.weights_g2h) + self.b_g2)	
        # Memory content 
        ht_c = self.activation( tf.matmul(fc2, self.weights_g3) + tf.matmul(rt * state[0], self.weights_g3h) + self.b_g3)
        # New hidden state
        ht = (1-zt) * state[0] + zt * ht_c
        ht = self.drop3(ht,training)

        return ht, [ht]

class Attention(Layer):
    '''
    Attention Mechanism
    '''
    def __init__(self, units, score_function='tanh'):
        super(Attention, self).__init__()
        self.W1 = tf.keras.layers.Dense(units)
        self.V = tf.keras.layers.Dense(1)
        self.score_function = score_function
    
    def call(self, query):
        score = self.V( tf.math.tanh( self.W1(query) ))
        if self.score_function == 'softmax':
            attention_weights = tf.nn.softmax(score,1)
        elif self.score_function == 'tanh':
            attention_weights = tf.math.tanh(score)
        context_vector = attention_weights * query
        context_vector = tf.reduce_sum(context_vector, axis=1)

        return context_vector, attention_weights

class RNN_Branch (Layer):
    '''
    RNN Branch with FCGRU and attention mechanism
    '''
    def __init__(self, drop, units, fc_units):
        super(RNN_Branch, self).__init__()
        fcgru = FCGRU(units,fc_units,drop)
        self.rnn_cell = RNN(fcgru,return_sequences=True)
        self.attention = Attention(units)
    
    def call(self,inputs,is_training):
        H = self.rnn_cell(inputs,training=is_training)
        feat, scores = self.attention(H)
        return feat, scores

class FC(Layer):
    '''
    Dense layer with batch normalization 
    '''
    def __init__(self,num_units,drop,act='relu'):
        super(FC,self).__init__()
        self.dense = Dense(num_units)
        self.bn = BatchNormalization()
        self.act = Activation(act)
        self.drop_layer = Dropout(rate=drop)
    def call(self,inputs,is_training):
        return self.drop_layer(self.act(self.bn( self.dense(inputs) ) ), training=is_training)

class HOb2sRNN_Model (tf.keras.Model):
    '''
    HOb2sRNN model without hierarchy
    '''
    def __init__(self, n_classes, drop=0.4, units=256, fc_units=64, classif_units=512):
        super(HOb2sRNN_Model, self).__init__(name='HOb2sRNN')
        self.branch = RNN_Branch(drop,units,fc_units)
        self.fc1 = FC (classif_units,drop)
        self.fc2 = FC (classif_units,drop)
        self.output_layer = Dense (n_classes,activation='softmax')

    def call (self, inputs, is_training):
        feat, _ = self.branch(inputs,is_training)
        feat = self.fc2(self.fc1(feat,is_training),is_training)
        return self.output_layer(feat)