import tensorflow as tf
from tensorflow.keras.layers import Layer, Activation, Dense, Dropout, BatchNormalization, Conv1D, Flatten
tf.keras.backend.set_floatx('float32')

class ConvBlock(Layer):
    '''
    1D Convolution block with batch normalization and dropout layer
    '''
    def __init__(self, n_filters, k_size, drop, padding_mode='valid'):
        super(ConvBlock, self).__init__()
        self.conv = Conv1D(filters=n_filters, kernel_size=k_size, padding=padding_mode, 
                            kernel_regularizer=tf.keras.regularizers.l2(l=1E-6))
        self.bn = BatchNormalization()
        self.activation = Activation('relu')
        self.drop_layer = Dropout(rate=drop)
    
    def call (self,inputs,is_training):
        conv = self.conv(inputs)
        conv = self.bn(conv)
        conv = self.activation(conv)
        conv = self.drop_layer(conv,training=is_training)
        return conv

class TempCNN_Encoder (Layer):
    '''
    TempCNN encoder from (Pelletier et al, 2019) 
    https://www.mdpi.com/2072-4292/11/5/523
    '''
    def __init__(self, n_filters, drop):
        super(TempCNN_Encoder, self).__init__()
        self.block1 = ConvBlock(n_filters,5,drop)
        self.block2 = ConvBlock(n_filters,5,drop)
        self.block3 = ConvBlock(n_filters,5,drop)
        self.flatten = Flatten()
    
    def call(self,inputs, is_training):
        b1 = self.block1(inputs,is_training)
        b2 = self.block2(b1,is_training)
        b3 = self.block3(b2,is_training)
        flatten = self.flatten(b3)
        return flatten
    
class TempCNN_Model (tf.keras.Model):
    '''
    TempCNN model with output layer
    '''
    def __init__(self, n_classes, drop=0.5, n_filters=64, n_units=256):
        super(TempCNN_Model, self).__init__(name='TempCNN')
        self.branch = TempCNN_Encoder(n_filters,drop)
        self.dense = Dense(n_units,kernel_regularizer=tf.keras.regularizers.l2(l=1E-6))
        self.bn = BatchNormalization()
        self.activation = Activation('relu')
        self.drop_layer = Dropout(rate=drop)
        self.output_layer = Dense (n_classes,activation='softmax',
                                    kernel_regularizer=tf.keras.regularizers.l2(l=1E-6))

    def call (self, inputs, is_training):
        feat = self.branch(inputs,is_training)
        feat = self.drop_layer(self.activation(self.bn(self.dense(feat))),is_training)
        return self.output_layer(feat)